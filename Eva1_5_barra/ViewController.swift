//
//  ViewController.swift
//  Eva1_5_barra
//
//  Created by TEMPORAL2 on 02/09/16.
//  Copyright © 2016 TEMPORAL2. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var lblMuestra: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func changeValue(sender: UISlider) {
        lblMuestra.text = "\(lroundf(sender.value))"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

